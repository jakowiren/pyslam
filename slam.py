#!/usr/bin/env python3
#4 hr 2 min
from multiprocessing.dummy import Process
from nis import match
import cv2
import g2o
from display import Display
import numpy as np
from Frame import FrameClass, match_frames
from helpers import denormalize
import OpenGL.GL as gl
import pangolin
from pointmap import Map, Point
import os

#camera intrinsics
H, W = 720, 1280 
# TODO: find a way to negative mask the car!!
F = 365 #focal length - how many pixels change when rotating camera 1 rad
K = np.array([[F,0,W//2], [0,F,H//2], [0, 0, 1]]) #instrinsics matrix

from multiprocessing import process, Queue
'''
F has been estimated. 
from svd:
f_est = np.sqrt(2)/((v[0] + v[1])/2)
self.f_est_avg.append(f_est)
print(f_est, np.median(self.f_est_avg)) way to estimate focal length
'''


#classes
#disp = Display(W,H)
mapp = Map()
disp = Display(W,H) if os.getenv("D2D") is not None else None

def triangulate(pose1, pose2, pts1, pts2):
  return cv2.triangulatePoints(pose1[:3], pose2[:3], pts1.T, pts2.T).T

'''
ORB(Oriented FAST and Rotated BRIEF) technique uses the FAST 
algorithm to calculate the keypoints. 
FAST stands for Features from Accelerated Segments Test. 
FAST calculates keypoints by considering pixel 
brightness around a given area.
'''


def process_frame(img):  #process frame
    img = cv2.resize(img, (W,H))
    frame = FrameClass(mapp, img, K)
    if frame.id == 0:
        return
    
    f1 = mapp.frames[-1]
    f2 = mapp.frames[-2]

    idx1,idx2,Rt = match_frames(f1,f2)
    f1.pose = np.dot(Rt, f2.pose)

    # homogeneous 3-D coords
    pts4d = triangulate(f1.pose, f2.pose, f1.pts[idx1], f2.pts[idx2])
    pts4d /= pts4d[:, 3:]

    # reject pts without enough "parallax" (this right?)
    # reject points behind the camera
    good_pts4d = (np.abs(pts4d[:, 3]) > 0.005) & (pts4d[:, 2] > 0)

    for i,p in enumerate(pts4d):
        if not good_pts4d[i]:
            continue
        pt = Point(p)
        pt.add_observation(f1, idx1[i])
        pt.add_observation(f2, idx2[i])
        '''
        for p in kps:
            u,v = map(lambda x: int(round(x)), p.pt)
            cv2.circle(img, (u,v), color=(0,255,0), radius = 2 )
        '''
        for pt1, pt2 in zip(f1.pts[idx1], f2.pts[idx2]):
            u1, v1 = denormalize(K, pt1)
            u2, v2 = denormalize(K, pt2)
            cv2.circle(img, (u1,v1), color=(0,255,0), radius = 2) #bgr
            cv2.line(img, (u1,v1), (u2,v2), (255,0,0)) 
    
    #2D disp
    if disp is not None:        
        disp.draw(img)
    
    #3D disp
    mapp.display()



if __name__ == '__main__':
    cap = cv2.VideoCapture('video.mp4')

    while cap.isOpened():
        ret, frame = cap.read() #capture images
        if ret == True: 
            process_frame(frame)
        else: 
            break