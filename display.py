import sdl2
import sdl2.ext
'''
Display the video and keypoints
'''
class Display(object):
    def __init__(self, W, H) -> None:
        sdl2.ext.init()

        self.W, self.H = W,H
        self.window = sdl2.ext.Window('slam', size=(W,H), position=(0,0))
        self.window.show()

    def draw(self, img):
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                exit(0)
        
        surf = sdl2.ext.pixels3d(self.window.get_surface())
        surf[:, :, 0:3] = img.swapaxes(0,1) #Blue rather than black
        
        #blit
        #sdl2.SDL_Delay(1) #just for visualisation
        self.window.refresh()