from contextlib import suppress
import cv2
import numpy as np
np.set_printoptions(suppress=True)

from skimage.measure import ransac
from skimage.transform import EssentialMatrixTransform, FundamentalMatrixTransform
from helpers import normalize, fundamentalToRt


def extractFeatures( img): 
        orb = cv2.ORB_create(1000) #vs sift? 
        #detection
        pts = cv2.goodFeaturesToTrack(np.mean(img, axis=2).astype(np.uint8), 1000, qualityLevel=0.01, minDistance=2)
        #extraction
        kps = [cv2.KeyPoint(x=f[0][0], y=f[0][1], size=20) for f in pts]
        kps, des = orb.compute(img, kps)

        #ret pts, des
        return np.array([(kp.pt[0], kp.pt[1]) for kp in kps]), des   

def match_frames(f1, f2):

    bf = cv2. BFMatcher(cv2.NORM_HAMMING)
    matches = bf.knnMatch(f1.des,f2.des, k=2) #returns 2 best matches
        
    #Lowe's ratio test
    ret = []
    idx1, idx2 = [], []
    for m,n in matches:
        if m.distance < 0.75*n.distance: #Lowe's ratio test
            idx1.append(m.queryIdx)
            idx2.append(m.trainIdx)

            p1 = f1.pts[m.queryIdx]
            p2 = f1.pts[m.trainIdx]

            ret.append((p1,p2))
    
    # no duplicates
    assert(len(set(idx1)) == len(idx1))
    assert(len(set(idx2)) == len(idx2))
    assert len(ret)>=8
    ret = np.array(ret)
    idx1 = np.array(idx1)
    idx2 = np.array(idx2)

    #normalize coords for essentialmatrixtransform, estimates less parameters?

    #ret[:,0, :] = normalize(ret[:,0, :])
    #ret[:,1, :] = normalize(ret[:,1, :])
    
    #fit matrix
    model, inliers = ransac(
                            (ret[:,0],#make class variables
                            ret[:,1]),
                            EssentialMatrixTransform, 
                            min_samples=8, 
                            residual_threshold=0.001, 
                            max_trials=200
                            )
    print("Matches:  %d -> %d -> %d -> %d" % (len(f1.des), len(matches), len(inliers), sum(inliers)))
    return idx1[inliers], idx2[inliers], fundamentalToRt(model.params)


class FrameClass(object):
    IRt = np.eye(4)

    def __init__(self,mapp, img, K) -> None:
        self.K = K
        self.pose = self.IRt
        self.pts, self.des = extractFeatures(img)
        self.Kinv = np.linalg.inv(self.K)
        self.pts = normalize(self.Kinv, self.pts)
        self.id = len(mapp.frames)
        mapp.frames.append(self)

    def annotate():
        pass
    '''
    
    @property
    def Kinv(self):
        if not hasattr(self, '_Kinv'):
            self._Kinv = np.linalg.inv(self.K)
        return self._Kinv

        # normalized keypoints
    @property
    def kps(self):
        if not hasattr(self, '_kps'):
            self._kps = normalize(self.Kinv, self.kps)
        return self._kps
    '''
    


'''
By estimating the camera projection matrix and 
fundamental matrix through the 8-point algorithm and RANSAC,
we can filter out incorrect matches that do not correspond
with the calculated and estimated fundamental matrix,
thereby increasing the overall accuracy and confidence of our matches. 
'''




'''  
#split image into chunks for even distr of kp and features
#currently gives weird distr of kp 
imgheight = img.shape[0]
imgwidth = img.shape[1]

sy = imgheight//self.GY
sx = imgwidth//self.GX
akp = [] # all key points
for ry in range (0, imgheight, sy):
for rx in range (0, imgwidth, sx):
img_chunk = img[ry:ry+sy, rx:rx+sx]
kp = self.orb.detect(img_chunk, None)
for p in kp:
p.pt = (p.pt[0] + rx, p.pt[1] + ry)
akp.append(p)
return akp
'''